#![no_std]
#![no_main]

use panic_halt as _;

use cortex_m::asm;

use nb::block;

use cortex_m_rt::entry;
use stm32f1xx_hal::{
    pac,
    prelude::*,
    serial::{Config, Serial},
};

#[entry]
fn main() -> ! {
    let p = crate::pac::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();
    let mut flash = p.FLASH.constrain();
    let mut rcc = p.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);
    let mut afio = p.AFIO.constrain(&mut rcc.apb2);
    let mut gpioa = p.GPIOA.split(&mut rcc.apb2);

    let pin_tx = gpioa.pa9.into_alternate_push_pull(&mut gpioa.crh);
    let pin_rx = gpioa.pa10;
    let serial = Serial::usart1(
       p.USART1,
       (pin_tx, pin_rx),
       &mut afio.mapr,
       Config::default().baudrate(9_600.bps()),
       clocks,
       &mut rcc.apb2,
    );

    let (mut tx, mut rx) = serial.split();

    block!(tx.write(b'J')).ok();
    let received = block!(rx.read()).unwrap();

    loop {} // NOP
}
